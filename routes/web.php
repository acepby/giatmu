<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'GiatMapController@index');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/acara','AcaraController@index')->name('acara');

/*
 * Giatmus Routes
 */
Route::get('/giatmus-all', 'GiatMapController@index')->name('giatmu_map.index');
Route::resource('giatmus', 'GiatmuController');

/*
 * GiatmuCategories Routes
 */
Route::resource('giatmu_categories', 'GiatmuCategoryController');
