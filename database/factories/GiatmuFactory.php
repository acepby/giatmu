<?php

use App\User;
use App\Giatmu;
use Faker\Generator as Faker;

$factory->define(Giatmu::class, function (Faker $faker) {
  $mapCenterLatitude = config('leaflet.map_center_latitude');
  $mapCenterLongitude = config('leaflet.map_center_longitude');
  $minLatitude = $mapCenterLatitude - 0.5;
  $maxLatitude = $mapCenterLatitude + 0.5;
  $minLongitude = $mapCenterLongitude - 0.7;
  $maxLongitude = $mapCenterLongitude + 0.7;
    return [
        'name' => $faker->word,
        'description' => $faker->sentence,
        'giatmu_category_id' => mt_rand(1,6),
        'tanggal'     =>$faker->date,
        'lokasi'      =>$faker->address,
        'url'         =>$faker->url,
        'latitude'    =>$faker->latitude($minLatitude, $maxLatitude),
        'longitude'   =>$faker->longitude($minLongitude, $maxLongitude),
        'creator_id' => function () {
            return factory(User::class)->create()->id;
        },
    ];
});
