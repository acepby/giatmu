<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Acara;
use App\User;
use Faker\Generator as Faker;

$factory->define(Acara::class, function (Faker $faker) {
    $mapCenterLatitude = config('leaflet.map_center_latitude');
    $mapCenterLongitude = config('leaflet.map_center_longitude');
    $minLatitude = $mapCenterLatitude - 0.5;
    $maxLatitude = $mapCenterLatitude + 0.5;
    $minLongitude = $mapCenterLongitude - 0.7;
    $maxLongitude = $mapCenterLongitude + 0.7;
    return [
        'nama'        =>$faker->sentence($nbWords = 6, $variableNbWords = true),
        'description' =>$faker->text($maxNbChars = 200),
        'tanggal'     =>$faker->date,
        'lokasi'      =>$faker->address,
        'url'         =>$faker->url,
        'latitude'    =>$faker->latitude($minLatitude, $maxLatitude),
        'longitude'   =>$faker->longitude($minLongitude, $maxLongitude),
        'user_id'  => function(){
            return factory(User::class)->create()->id;
        },
    ];
});
