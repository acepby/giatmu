<?php

return [
    // Labels
    'giatmu'         => 'Giat',
    'list'           => 'List Giatmu',
    'search'         => 'Search Giatmu',
    'search_text'    => 'Name ...',
    'all'            => 'All Giatmu',
    'select'         => 'Select Giatmu',
    'detail'         => 'Giatmu Detail',
    'not_found'      => 'Giatmu not found.',
    'empty'          => 'Giatmu is empty.',
    'back_to_show'   => 'Back to Giatmu Detail',
    'back_to_index'  => 'Back to Giatmu List',

    // Actions
    'create'         => 'Create Giatmu',
    'created'        => 'A new Giatmu has been created.',
    'show'           => 'View Giatmu Detail',
    'edit'           => 'Edit Giatmu',
    'update'         => 'Update Giatmu',
    'updated'        => 'Giatmu data has been updated.',
    'delete'         => 'Delete Giatmu',
    'delete_confirm' => 'Are you sure to delete this Giatmu?',
    'deleted'        => 'Giatmu has been deleted.',
    'undeleted'      => 'Giatmu not deleted.',
    'undeleteable'   => 'Giatmu data cannot be deleted.',

    // Attributes
    'name'           => 'Nama Kegiatan',
    'description'    => 'Deskripsi',
    'tanggal'        => 'Tanggal',
    'lokasi'        => 'Lokasi',
    'coordinate'    => 'Kordinat',
    'category'      => 'Kategori',
    'url'           => 'Link Berita',
    'latitude'      => 'Latitude',
    'longitude'     => 'Longitude',
];
