<?php

return [
    // Labels
    'giatmu_category'     => 'Giatmu Category',
    'list'           => 'Giatmu Category List',
    'search'         => 'Search Giatmu Category',
    'search_text'    => 'Name ...',
    'all'            => 'All Giatmu Category',
    'select'         => 'Select Giatmu Category',
    'detail'         => 'Giatmu Category Detail',
    'not_found'      => 'Giatmu Category not found.',
    'empty'          => 'Giatmu Category is empty.',
    'back_to_show'   => 'Back to Giatmu Category Detail',
    'back_to_index'  => 'Back to Giatmu Category List',

    // Actions
    'create'         => 'Create new Giatmu Category',
    'created'        => 'A new Giatmu Category has been created.',
    'show'           => 'View Giatmu Category Detail',
    'edit'           => 'Edit Giatmu Category',
    'update'         => 'Update Giatmu Category',
    'updated'        => 'Giatmu Category data has been updated.',
    'delete'         => 'Delete Giatmu Category',
    'delete_confirm' => 'Are you sure to delete this Giatmu Category?',
    'deleted'        => 'Giatmu Category has been deleted.',
    'undeleted'      => 'Giatmu Category not deleted.',
    'undeleteable'   => 'Giatmu Category data cannot be deleted.',

    // Attributes
    'name'           => 'Giatmu Category Name',
    'description'    => 'Giatmu Category Description',
];
