@extends('layouts.app')

@section('title', __('giatmu.edit'))

@section('content')
<div class="row justify-content-center">
    <div class="col-md-6">
        @if (request('action') == 'delete' && $giatmu)
        @can('delete', $giatmu)
            <div class="card">
                <div class="card-header">{{ __('giatmu.delete') }}</div>
                <div class="card-body">
                    <label class="control-label text-primary">{{ __('giatmu.name') }}</label>
                    <p>{{ $giatmu->name }}</p>
                    <label class="control-label text-primary">{{ __('giatmu.lokasi') }}</label>
                    <p>{{ $giatmu->lokasi }}</p>
                    <label class="control-label text-primary">{{ __('giatmu.latitude') }}</label>
                    <p>{{ $giatmu->latitude }}</p>
                    <label class="control-label text-primary">{{ __('giatmu.longitude') }}</label>
                    <p>{{ $giatmu->longitude }}</p>
                    {!! $errors->first('giatmu_id', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                </div>
                <hr style="margin:0">
                <div class="card-body text-danger">{{ __('giatmu.delete_confirm') }}</div>
                <div class="card-footer">
                    <form method="POST" action="{{ route('giatmus.destroy', $giatmu) }}" accept-charset="UTF-8" onsubmit="return confirm(&quot;{{ __('app.delete_confirm') }}&quot;)" class="del-form float-right" style="display: inline;">
                        {{ csrf_field() }} {{ method_field('delete') }}
                        <input name="giatmu_id" type="hidden" value="{{ $giatmu->id }}">
                        <button type="submit" class="btn btn-danger">{{ __('app.delete_confirm_button') }}</button>
                    </form>
                    <a href="{{ route('giatmus.edit', $giatmu) }}" class="btn btn-link">{{ __('app.cancel') }}</a>
                </div>
            </div>
        @endcan
        @else
        <div class="card">
            <div class="card-header">{{ __('giatmu.edit') }}</div>
            <form method="POST" action="{{ route('giatmus.update', $giatmu) }}" accept-charset="UTF-8">
                {{ csrf_field() }} {{ method_field('patch') }}
                <div class="card-body">

                    <div class="form-group">
                        <label for="name" class="control-label">{{ __('giatmu.name') }}</label>
                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $giatmu->name) }}" required>
                        {!! $errors->first('name', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="description" class="control-label">{{ __('giatmu.description') }}</label>
                        <textarea id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" rows="4">{{ old('description',$giatmu->description) }}</textarea>
                        {!! $errors->first('description', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="giatmu_category_id" class="control-label">{{ __('giatmu_category.name') }}</label>
                        <select name="giatmu_category_id" class="form-control{{ $errors->has('giatmu_category_id')? ' is-invalid' : ''}}" name="giatmu_category_id" required>
                          @if(count($data['categories']) > 0) @foreach($data['categories'] as $category)
                            <option value="{{$category->id}}" {{ old('giatmu_category_id', $giatmu->giatmu_category_id) == $category->id ? 'selected' : '' }}>{{$category->name}}</option>
                          @endforeach @endif
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="tanggal" class="control-label">{{ __('giatmu.tanggal') }}</label>
                        <input id="tanggal" type="date" class="form-control{{ $errors->has('tanggal') ? ' is-invalid' : '' }}" name="tanggal" value="{{ old('tanggal',$giatmu->tanggal) }}" required>
                        {!! $errors->first('tanggal', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>

                    <div class="form-group">
                        <label for="lokasi" class="control-label">{{ __('giatmu.lokasi') }}</label>
                        <textarea id="lokasi" class="form-control{{ $errors->has('lokasi') ? ' is-invalid' : '' }}" name="lokasi" rows="4">{{ old('lokasi', $giatmu->lokasi) }}</textarea>
                        {!! $errors->first('lokasi', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="url" class="control-label">{{ __('giatmu.url') }}</label>
                        <input id="url" type="text" class="form-control{{ $errors->has('url') ? ' is-invalid' : '' }}" name="url" value="{{ old('url',$giatmu->url) }}" required>
                        {!! $errors->first('url', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="latitude" class="control-label">{{ __('giatmu.latitude') }}</label>
                                <input id="latitude" type="text" class="form-control{{ $errors->has('latitude') ? ' is-invalid' : '' }}" name="latitude" value="{{ old('latitude', $giatmu->latitude) }}" required>
                                {!! $errors->first('latitude', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="longitude" class="control-label">{{ __('giatmu.longitude') }}</label>
                                <input id="longitude" type="text" class="form-control{{ $errors->has('longitude') ? ' is-invalid' : '' }}" name="longitude" value="{{ old('longitude', $giatmu->longitude) }}" required>
                                {!! $errors->first('longitude', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                    <div id="mapid"></div>
                </div>
                <div class="card-footer">
                    <input type="submit" value="{{ __('giatmu.update') }}" class="btn btn-success">
                    <a href="{{ route('giatmus.show', $giatmu) }}" class="btn btn-link">{{ __('app.cancel') }}</a>
                    @can('delete', $giatmu)
                        <a href="{{ route('giatmus.edit', [$giatmu, 'action' => 'delete']) }}" id="del-giatmu-{{ $giatmu->id }}" class="btn btn-danger float-right">{{ __('app.delete') }}</a>
                    @endcan
                </div>
            </form>
        </div>
    </div>
</div>
@endif
@endsection

@section('styles')
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css"
    integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ=="
    crossorigin=""/>

<style>
    #mapid { height: 300px; }
</style>
@endsection

@push('scripts')
<script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"
    integrity="sha512-/Nsx9X4HebavoBvEBuyp3I7od5tA0UzAxs+j83KgC8PU0kgB4XiK4Lfe4y4cgBtaRJQEIFCW+oC506aPT2L1zw=="
    crossorigin=""></script>
<script>
    var mapCenter = [{{ $giatmu->latitude }}, {{ $giatmu->longitude }}];
    var map = L.map('mapid').setView(mapCenter, {{ config('leaflet.detail_zoom_level') }});

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    var marker = L.marker(mapCenter).addTo(map);
    function updateMarker(lat, lng) {
        marker
        .setLatLng([lat, lng])
        .bindPopup("Your location :  " + marker.getLatLng().toString())
        .openPopup();
        return false;
    };

    map.on('click', function(e) {
        let latitude = e.latlng.lat.toString().substring(0, 15);
        let longitude = e.latlng.lng.toString().substring(0, 15);
        $('#latitude').val(latitude);
        $('#longitude').val(longitude);
        updateMarker(latitude, longitude);
    });

    var updateMarkerByInputs = function() {
        return updateMarker( $('#latitude').val() , $('#longitude').val());
    }
    $('#latitude').on('input', updateMarkerByInputs);
    $('#longitude').on('input', updateMarkerByInputs);
</script>
@endpush
