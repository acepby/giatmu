@extends('layouts.app')

@section('title', __('giatmu.list'))

@section('content')
<div class="mb-3">
    <div class="float-right">
        @can('create', new App\Giatmu)
            <a href="{{ route('giatmus.create') }}" class="btn btn-success">{{ __('giatmu.create') }}</a>
        @endcan
    </div>
    <h1 class="page-title">{{ __('giatmu.list') }} <small>{{ __('app.total') }} : {{ $giatmus->total() }} {{ __('giatmu.giatmu') }}</small></h1>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <form method="GET" action="" accept-charset="UTF-8" class="form-inline">
                    <div class="form-group">
                        <label for="q" class="form-label">{{ __('giatmu.search') }}</label>
                        <input placeholder="{{ __('giatmu.search_text') }}" name="q" type="text" id="q" class="form-control mx-sm-2" value="{{ request('q') }}">
                    </div>
                    <input type="submit" value="{{ __('giatmu.search') }}" class="btn btn-secondary">
                    <a href="{{ route('giatmus.index') }}" class="btn btn-link">{{ __('app.reset') }}</a>
                </form>
            </div>
            <table class="table table-sm table-responsive-sm table-hover">
                <thead>
                    <tr>
                        <th class="text-center">{{ __('app.table_no') }}</th>
                        <th>{{ __('giatmu.name') }}</th>
                        <th>{{ __('giatmu.description') }}</th>
                        <th>{{ __('giatmu.tanggal') }}</th>
                        <th>{{ __('giatmu.lokasi') }}</th>
                        <th class="text-center">{{ __('app.action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($giatmus as $key => $giatmu)
                    <tr>
                        <td class="text-center">{{ $giatmus->firstItem() + $key }}</td>
                        <td>{{ $giatmu->name }}</td>
                        <td>{{ $giatmu->description }}</td>
                        <td>{{ $giatmu->tanggal }}</td>
                        <td>{{ $giatmu->lokasi }}</td>
                        <td class="text-center">
                          <a href="{{ route('giatmus.show', $giatmu) }}" id="show-giatmu-{{ $giatmu->id }}">{{ __('app.show') }}</a>

                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="card-body">{{ $giatmus->appends(Request::except('page'))->render() }}</div>
        </div>
    </div>
    <!-- <div class="col-md-4">
        @if(Request::has('action'))
        @include('giatmus.forms')
        @endif
    </div> -->
</div>
@endsection
