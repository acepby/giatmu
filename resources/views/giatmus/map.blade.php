@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-body" id="mapid"></div>
</div>
@endsection

@section('styles')
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css"
    integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ=="
    crossorigin=""/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet.markercluster/1.4.1/MarkerCluster.css" integrity="sha256-+bdWuWOXMFkX0v9Cvr3OWClPiYefDQz9GGZP/7xZxdc=" crossorigin="anonymous" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet.markercluster/1.4.1/MarkerCluster.Default.css" integrity="sha256-LWhzWaQGZRsWFrrJxg+6Zn8TT84k0/trtiHBc6qcGpY=" crossorigin="anonymous" />
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" type="text/css">
<link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-fullscreen/v1.0.1/leaflet.fullscreen.css' rel='stylesheet' />

<style>
    #mapid { min-height: 500px; }
</style>
@endsection
@push('scripts')
<!-- Make sure you put this AFTER Leaflet's CSS -->
<script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"
    integrity="sha512-/Nsx9X4HebavoBvEBuyp3I7od5tA0UzAxs+j83KgC8PU0kgB4XiK4Lfe4y4cgBtaRJQEIFCW+oC506aPT2L1zw=="
    crossorigin=""></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet.markercluster/1.4.1/leaflet.markercluster.js" integrity="sha256-WL6HHfYfbFEkZOFdsJQeY7lJG/E5airjvqbznghUzRw=" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/leaflet.markercluster.layersupport@2.0.1/dist/leaflet.markercluster.layersupport.js"></script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>

    <script src="https://cdn.jsdelivr.net/gh/dwilhelm89/LeafletSlider@1.0.5/SliderControl.js"></script>
    <script src='https://api.mapbox.com/mapbox.js/plugins/leaflet-fullscreen/v1.0.1/Leaflet.fullscreen.min.js'></script>
<script>
  const start = async()=>{
    const tiles = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
    const map = new L.Map("mapid",{
      center: [{{ config('leaflet.map_center_latitude') }}, {{ config('leaflet.map_center_longitude') }}],
      zoom : 5,
      layers: [L.tileLayer(tiles,{attribution:'&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'})]
     });
    map.addControl(new L.Control.Fullscreen());

    var mcg,mcgls,allPoint,sliderControl;

    async function showData() {
      const response = await axios({
        method: "GET",
        url: '{{ route('api.giatmus.index') }}'
      });
      map.eachLayer(layer=>{
        if(!layer._url){
            layer.remove();
        }
      });
      allPoint= L.geoJSON(response.data,{
        onEachFeature: function(feature,layer){
            layer.bindPopup(feature.properties.map_popup_content);
        },
	pointToLayer: function(feature,latlng){
		var giatmuIcon= new L.Icon({
		    iconSize:[39,32],
		    iconAnchor:[19,32],
		    popupAnchor:[0,-28],
		    iconUrl:"{{ URL::to('/') }}/images/giatmu/"+feature.properties.giatmu_category_id+".png"
		})
		return L.marker(latlng,{icon:giatmuIcon});
	}
      });
      mcg = L.markerClusterGroup();
      mcgls = L.markerClusterGroup.layerSupport();
      mcgls.addTo(map).checkIn(allPoint);

      sliderControl = L.control.sliderControl({
                          position: "topright",
                          layer: allPoint,
                          timeAttribute:'tanggal',
                          showAllOnStart: true,
                          range: true
                          });
      map.addControl(sliderControl);

      sliderControl.options.markers.sort(function(a, b) {
            //console.log(a.feature.properties.giatmu_category_id);
            return (a.feature.properties.tanggal > b.feature.properties.tanggal);
          });
      sliderControl.startSlider();
      //map.addControl(new L.Control.Fullscreen());
    }
    showData();
    setInterval(async()=>{
    showData()}, 300000)
  }
  start();
    /**
    var map = L.map('mapid').setView([{{ config('leaflet.map_center_latitude') }}, {{ config('leaflet.map_center_longitude') }}], 5);
    var baseUrl = "{{ url('/') }}";
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);
    var mcg = L.markerClusterGroup();
    var mcgls = L.markerClusterGroup.layerSupport();
    //const axios = require('axios');
    axios.get('{{ route('api.giatmus.index') }}')
    .then(function (response) {
        console.log(response.data);

        var allPoint =L.geoJSON(response.data, {
            //pointToLayer: function(geoJsonPoint, latlng) {
             //   return L.marker(latlng);
            //}
            onEachFeature: function(feature,layer){
                layer.bindPopup(feature.properties.map_popup_content);
            }
        });
        //.bindPopup(function (layer) {
        //    return layer.feature.properties.map_popup_content;
        //});
        //allPoint.addTo(map);
        //mcg.addLayer(allPoint);
        mcgls.addTo(map).checkIn(allPoint);
        //map.addLayer(mcg);
        //allPoint.addTo(map);

        var sliderControl = L.control.sliderControl({
                            position: "topright",
                            layer: allPoint,
                            timeAttribute:'tanggal',
                            showAllOnStart: true,
                            range: true
                            });

        map.addControl(sliderControl);

        sliderControl.options.markers.sort(function(a, b) {
                        //console.log(a.feature.properties.tanggal);
                        return (a.feature.properties.tanggal > b.feature.properties.tanggal);
                    });
        sliderControl.startSlider();

    })
    .catch(function (error) {
        console.log(error);
    });


    @can('create', new App\Giatmu)
    var theMarker;
    map.on('click', function(e) {
        let latitude = e.latlng.lat.toString().substring(0, 15);
        let longitude = e.latlng.lng.toString().substring(0, 15);
        if (theMarker != undefined) {
            map.removeLayer(theMarker);
        };
        var popupContent = "Your location : " + latitude + ", " + longitude + ".";
        popupContent += '<br><a href="{{ route('giatmus.create') }}?latitude=' + latitude + '&longitude=' + longitude + '">Add new outlet here</a>';
        theMarker = L.marker([latitude, longitude]).addTo(map);
        theMarker.bindPopup(popupContent)
        .openPopup();
    });
    @endcan
    **/
</script>
@endpush
