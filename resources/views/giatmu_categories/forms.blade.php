@if (Request::get('action') == 'create')
@can('create', new App\GiatmuCategory)
    <form method="POST" action="{{ route('giatmu_categories.store') }}" accept-charset="UTF-8">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="name" class="form-label">{{ __('giatmu_category.name') }} <span class="form-required">*</span></label>
            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required>
            {!! $errors->first('name', '<span class="invalid-feedback" role="alert">:message</span>') !!}
        </div>
        <div class="form-group">
            <label for="description" class="form-label">{{ __('giatmu_category.description') }}</label>
            <textarea id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" rows="4">{{ old('description') }}</textarea>
            {!! $errors->first('description', '<span class="invalid-feedback" role="alert">:message</span>') !!}
        </div>
        <input type="submit" value="{{ __('giatmu_category.create') }}" class="btn btn-success">
        <a href="{{ route('giatmu_categories.index') }}" class="btn btn-link">{{ __('app.cancel') }}</a>
    </form>
@endcan
@endif
@if (Request::get('action') == 'edit' && $editableGiatmuCategory)
@can('update', $editableGiatmuCategory)
    <form method="POST" action="{{ route('giatmu_categories.update', $editableGiatmuCategory) }}" accept-charset="UTF-8">
        {{ csrf_field() }} {{ method_field('patch') }}
        <div class="form-group">
            <label for="name" class="form-label">{{ __('giatmu_category.name') }} <span class="form-required">*</span></label>
            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $editableGiatmuCategory->name) }}" required>
            {!! $errors->first('name', '<span class="invalid-feedback" role="alert">:message</span>') !!}
        </div>
        <div class="form-group">
            <label for="description" class="form-label">{{ __('giatmu_category.description') }}</label>
            <textarea id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" rows="4">{{ old('description', $editableGiatmuCategory->description) }}</textarea>
            {!! $errors->first('description', '<span class="invalid-feedback" role="alert">:message</span>') !!}
        </div>
        <input name="page" value="{{ request('page') }}" type="hidden">
        <input name="q" value="{{ request('q') }}" type="hidden">
        <input type="submit" value="{{ __('giatmu_category.update') }}" class="btn btn-success">
        <a href="{{ route('giatmu_categories.index', Request::only('q', 'page')) }}" class="btn btn-link">{{ __('app.cancel') }}</a>
        @can('delete', $editableGiatmuCategory)
            <a href="{{ route('giatmu_categories.index', ['action' => 'delete', 'id' => $editableGiatmuCategory->id] + Request::only('page', 'q')) }}" id="del-giatmu_category-{{ $editableGiatmuCategory->id }}" class="btn btn-danger float-right">{{ __('app.delete') }}</a>
        @endcan
    </form>
@endcan
@endif
@if (Request::get('action') == 'delete' && $editableGiatmuCategory)
@can('delete', $editableGiatmuCategory)
    <div class="card">
        <div class="card-header">{{ __('giatmu_category.delete') }}</div>
        <div class="card-body">
            <label class="form-label text-primary">{{ __('giatmu_category.name') }}</label>
            <p>{{ $editableGiatmuCategory->name }}</p>
            <label class="form-label text-primary">{{ __('giatmu_category.description') }}</label>
            <p>{{ $editableGiatmuCategory->description }}</p>
            {!! $errors->first('giatmu_category_id', '<span class="invalid-feedback" role="alert">:message</span>') !!}
        </div>
        <hr style="margin:0">
        <div class="card-body text-danger">{{ __('giatmu_category.delete_confirm') }}</div>
        <div class="card-footer">
            <form method="POST" action="{{ route('giatmu_categories.destroy', $editableGiatmuCategory) }}" accept-charset="UTF-8" onsubmit="return confirm(&quot;{{ __('app.delete_confirm') }}&quot;)" class="del-form float-right" style="display: inline;">
                {{ csrf_field() }} {{ method_field('delete') }}
                <input name="giatmu_category_id" type="hidden" value="{{ $editableGiatmuCategory->id }}">
                <input name="page" value="{{ request('page') }}" type="hidden">
                <input name="q" value="{{ request('q') }}" type="hidden">
                <button type="submit" class="btn btn-danger">{{ __('app.delete_confirm_button') }}</button>
            </form>
            <a href="{{ route('giatmu_categories.index', Request::only('q', 'page')) }}" class="btn btn-link">{{ __('app.cancel') }}</a>
        </div>
    </div>
@endcan
@endif
