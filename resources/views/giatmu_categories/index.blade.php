@extends('layouts.app')

@section('title', __('giatmu_category.list'))

@section('content')
<div class="mb-3">
    <div class="float-right">
        @can('create', new App\GiatmuCategory)
            <a href="{{ route('giatmu_categories.index', ['action' => 'create']) }}" class="btn btn-success">{{ __('giatmu_category.create') }}</a>
        @endcan
    </div>
    <h1 class="page-title">{{ __('giatmu_category.list') }} <small>{{ __('app.total') }} : {{ $giatmuCategories->total() }} {{ __('giatmu_category.giatmu_category') }}</small></h1>
</div>

<div class="row">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <form method="GET" action="" accept-charset="UTF-8" class="form-inline">
                    <div class="form-group">
                        <label for="q" class="form-label">{{ __('giatmu_category.search') }}</label>
                        <input placeholder="{{ __('giatmu_category.search_text') }}" name="q" type="text" id="q" class="form-control mx-sm-2" value="{{ request('q') }}">
                    </div>
                    <input type="submit" value="{{ __('giatmu_category.search') }}" class="btn btn-secondary">
                    <a href="{{ route('giatmu_categories.index') }}" class="btn btn-link">{{ __('app.reset') }}</a>
                </form>
            </div>
            <table class="table table-sm table-responsive-sm table-hover">
                <thead>
                    <tr>
                        <th class="text-center">{{ __('app.table_no') }}</th>
                        <th>{{ __('giatmu_category.name') }}</th>
                        <th>{{ __('giatmu_category.description') }}</th>
                        <th class="text-center">{{ __('app.action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($giatmuCategories as $key => $giatmuCategory)
                    <tr>
                        <td class="text-center">{{ $giatmuCategories->firstItem() + $key }}</td>
                        <td>{{ $giatmuCategory->name }}</td>
                        <td>{{ $giatmuCategory->description }}</td>
                        <td class="text-center">
                            @can('update', $giatmuCategory)
                                <a href="{{ route('giatmu_categories.index', ['action' => 'edit', 'id' => $giatmuCategory->id] + Request::only('page', 'q')) }}" id="edit-giatmu_category-{{ $giatmuCategory->id }}">{{ __('app.edit') }}</a>
                            @endcan
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="card-body">{{ $giatmuCategories->appends(Request::except('page'))->render() }}</div>
        </div>
    </div>
    <div class="col-md-4">
        @if(Request::has('action'))
        @include('giatmu_categories.forms')
        @endif
    </div>
</div>
@endsection
