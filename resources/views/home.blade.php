@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Selamat Datang, Ayo berkontribusi dengan mendaftarkan kegiatan Lawan Covid di daerahmu. <br>
                    Silakan akses menu <a href="{{ route('giatmus.index') }}"> List Giatmu </a> untuk melihat daftar kegiatan dan tambahkan kegiatanmu disana.<br>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
