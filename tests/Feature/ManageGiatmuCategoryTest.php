<?php

namespace Tests\Feature;

use App\GiatmuCategory;
use Tests\BrowserKitTest as TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ManageGiatmuCategoryTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_see_giatmu_category_list_in_giatmu_category_index_page()
    {
        $giatmuCategory = factory(GiatmuCategory::class)->create();

        $this->loginAsUser();
        $this->visitRoute('giatmu_categories.index');
        $this->see($giatmuCategory->name);
    }

    /** @test */
    public function user_can_create_a_giatmu_category()
    {
        $this->loginAsUser();
        $this->visitRoute('giatmu_categories.index');

        $this->click(__('giatmu_category.create'));
        $this->seeRouteIs('giatmu_categories.index', ['action' => 'create']);

        $this->submitForm(__('giatmu_category.create'), [
            'name'        => 'GiatmuCategory 1 name',
            'description' => 'GiatmuCategory 1 description',
        ]);

        $this->seeRouteIs('giatmu_categories.index');

        $this->seeInDatabase('giatmu_categories', [
            'name'        => 'GiatmuCategory 1 name',
            'description' => 'GiatmuCategory 1 description',
        ]);
    }

    private function getCreateFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'GiatmuCategory 1 name',
            'description' => 'GiatmuCategory 1 description',
        ], $overrides);
    }

    /** @test */
    public function validate_giatmu_category_name_is_required()
    {
        $this->loginAsUser();

        // name empty
        $this->post(route('giatmu_categories.store'), $this->getCreateFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_giatmu_category_name_is_not_more_than_60_characters()
    {
        $this->loginAsUser();

        // name 70 characters
        $this->post(route('giatmu_categories.store'), $this->getCreateFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_giatmu_category_description_is_not_more_than_255_characters()
    {
        $this->loginAsUser();

        // description 256 characters
        $this->post(route('giatmu_categories.store'), $this->getCreateFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    /** @test */
    public function user_can_edit_a_giatmu_category_within_search_query()
    {
        $this->loginAsUser();
        $giatmuCategory = factory(GiatmuCategory::class)->create(['name' => 'Testing 123']);

        $this->visitRoute('giatmu_categories.index', ['q' => '123']);
        $this->click('edit-giatmu_category-'.$giatmuCategory->id);
        $this->seeRouteIs('giatmu_categories.index', ['action' => 'edit', 'id' => $giatmuCategory->id, 'q' => '123']);

        $this->submitForm(__('giatmu_category.update'), [
            'name'        => 'GiatmuCategory 1 name',
            'description' => 'GiatmuCategory 1 description',
        ]);

        $this->seeRouteIs('giatmu_categories.index', ['q' => '123']);

        $this->seeInDatabase('giatmu_categories', [
            'name'        => 'GiatmuCategory 1 name',
            'description' => 'GiatmuCategory 1 description',
        ]);
    }

    private function getEditFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'GiatmuCategory 1 name',
            'description' => 'GiatmuCategory 1 description',
        ], $overrides);
    }

    /** @test */
    public function validate_giatmu_category_name_update_is_required()
    {
        $this->loginAsUser();
        $giatmu_category = factory(GiatmuCategory::class)->create(['name' => 'Testing 123']);

        // name empty
        $this->patch(route('giatmu_categories.update', $giatmu_category), $this->getEditFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_giatmu_category_name_update_is_not_more_than_60_characters()
    {
        $this->loginAsUser();
        $giatmu_category = factory(GiatmuCategory::class)->create(['name' => 'Testing 123']);

        // name 70 characters
        $this->patch(route('giatmu_categories.update', $giatmu_category), $this->getEditFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_giatmu_category_description_update_is_not_more_than_255_characters()
    {
        $this->loginAsUser();
        $giatmu_category = factory(GiatmuCategory::class)->create(['name' => 'Testing 123']);

        // description 256 characters
        $this->patch(route('giatmu_categories.update', $giatmu_category), $this->getEditFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    /** @test */
    public function user_can_delete_a_giatmu_category()
    {
        $this->loginAsUser();
        $giatmuCategory = factory(GiatmuCategory::class)->create();
        factory(GiatmuCategory::class)->create();

        $this->visitRoute('giatmu_categories.index', ['action' => 'edit', 'id' => $giatmuCategory->id]);
        $this->click('del-giatmu_category-'.$giatmuCategory->id);
        $this->seeRouteIs('giatmu_categories.index', ['action' => 'delete', 'id' => $giatmuCategory->id]);

        $this->seeInDatabase('giatmu_categories', [
            'id' => $giatmuCategory->id,
        ]);

        $this->press(__('app.delete_confirm_button'));

        $this->dontSeeInDatabase('giatmu_categories', [
            'id' => $giatmuCategory->id,
        ]);
    }
}
