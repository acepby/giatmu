<?php

namespace Tests\Feature;

use App\Giatmu;
use Tests\BrowserKitTest as TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ManageGiatmuTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_see_giatmu_list_in_giatmu_index_page()
    {
        $giatmu = factory(Giatmu::class)->create();

        $this->loginAsUser();
        $this->visitRoute('giatmus.index');
        $this->see($giatmu->name);
    }

    /** @test */
    public function user_can_create_a_giatmu()
    {
        $this->loginAsUser();
        $this->visitRoute('giatmus.index');

        $this->click(__('giatmu.create'));
        $this->seeRouteIs('giatmus.index', ['action' => 'create']);

        $this->submitForm(__('giatmu.create'), [
            'name'        => 'Giatmu 1 name',
            'description' => 'Giatmu 1 description',
        ]);

        $this->seeRouteIs('giatmus.index');

        $this->seeInDatabase('giatmus', [
            'name'        => 'Giatmu 1 name',
            'description' => 'Giatmu 1 description',
        ]);
    }

    private function getCreateFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'Giatmu 1 name',
            'description' => 'Giatmu 1 description',
        ], $overrides);
    }

    /** @test */
    public function validate_giatmu_name_is_required()
    {
        $this->loginAsUser();

        // name empty
        $this->post(route('giatmus.store'), $this->getCreateFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_giatmu_name_is_not_more_than_60_characters()
    {
        $this->loginAsUser();

        // name 70 characters
        $this->post(route('giatmus.store'), $this->getCreateFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_giatmu_description_is_not_more_than_255_characters()
    {
        $this->loginAsUser();

        // description 256 characters
        $this->post(route('giatmus.store'), $this->getCreateFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    /** @test */
    public function user_can_edit_a_giatmu_within_search_query()
    {
        $this->loginAsUser();
        $giatmu = factory(Giatmu::class)->create(['name' => 'Testing 123']);

        $this->visitRoute('giatmus.index', ['q' => '123']);
        $this->click('edit-giatmu-'.$giatmu->id);
        $this->seeRouteIs('giatmus.index', ['action' => 'edit', 'id' => $giatmu->id, 'q' => '123']);

        $this->submitForm(__('giatmu.update'), [
            'name'        => 'Giatmu 1 name',
            'description' => 'Giatmu 1 description',
        ]);

        $this->seeRouteIs('giatmus.index', ['q' => '123']);

        $this->seeInDatabase('giatmus', [
            'name'        => 'Giatmu 1 name',
            'description' => 'Giatmu 1 description',
        ]);
    }

    private function getEditFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'Giatmu 1 name',
            'description' => 'Giatmu 1 description',
        ], $overrides);
    }

    /** @test */
    public function validate_giatmu_name_update_is_required()
    {
        $this->loginAsUser();
        $giatmu = factory(Giatmu::class)->create(['name' => 'Testing 123']);

        // name empty
        $this->patch(route('giatmus.update', $giatmu), $this->getEditFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_giatmu_name_update_is_not_more_than_60_characters()
    {
        $this->loginAsUser();
        $giatmu = factory(Giatmu::class)->create(['name' => 'Testing 123']);

        // name 70 characters
        $this->patch(route('giatmus.update', $giatmu), $this->getEditFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_giatmu_description_update_is_not_more_than_255_characters()
    {
        $this->loginAsUser();
        $giatmu = factory(Giatmu::class)->create(['name' => 'Testing 123']);

        // description 256 characters
        $this->patch(route('giatmus.update', $giatmu), $this->getEditFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    /** @test */
    public function user_can_delete_a_giatmu()
    {
        $this->loginAsUser();
        $giatmu = factory(Giatmu::class)->create();
        factory(Giatmu::class)->create();

        $this->visitRoute('giatmus.index', ['action' => 'edit', 'id' => $giatmu->id]);
        $this->click('del-giatmu-'.$giatmu->id);
        $this->seeRouteIs('giatmus.index', ['action' => 'delete', 'id' => $giatmu->id]);

        $this->seeInDatabase('giatmus', [
            'id' => $giatmu->id,
        ]);

        $this->press(__('app.delete_confirm_button'));

        $this->dontSeeInDatabase('giatmus', [
            'id' => $giatmu->id,
        ]);
    }
}
