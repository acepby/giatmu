<?php

namespace Tests\Unit\Policies;

use App\Giatmu;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class GiatmuPolicyTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_create_giatmu()
    {
        $user = $this->createUser();
        $this->assertTrue($user->can('create', new Giatmu));
    }

    /** @test */
    public function user_can_view_giatmu()
    {
        $user = $this->createUser();
        $giatmu = factory(Giatmu::class)->create();
        $this->assertTrue($user->can('view', $giatmu));
    }

    /** @test */
    public function user_can_update_giatmu()
    {
        $user = $this->createUser();
        $giatmu = factory(Giatmu::class)->create();
        $this->assertTrue($user->can('update', $giatmu));
    }

    /** @test */
    public function user_can_delete_giatmu()
    {
        $user = $this->createUser();
        $giatmu = factory(Giatmu::class)->create();
        $this->assertTrue($user->can('delete', $giatmu));
    }
}
