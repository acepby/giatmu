<?php

namespace Tests\Unit\Policies;

use App\GiatmuCategory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class GiatmuCategoryPolicyTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_create_giatmu_category()
    {
        $user = $this->createUser();
        $this->assertTrue($user->can('create', new GiatmuCategory));
    }

    /** @test */
    public function user_can_view_giatmu_category()
    {
        $user = $this->createUser();
        $giatmuCategory = factory(GiatmuCategory::class)->create();
        $this->assertTrue($user->can('view', $giatmuCategory));
    }

    /** @test */
    public function user_can_update_giatmu_category()
    {
        $user = $this->createUser();
        $giatmuCategory = factory(GiatmuCategory::class)->create();
        $this->assertTrue($user->can('update', $giatmuCategory));
    }

    /** @test */
    public function user_can_delete_giatmu_category()
    {
        $user = $this->createUser();
        $giatmuCategory = factory(GiatmuCategory::class)->create();
        $this->assertTrue($user->can('delete', $giatmuCategory));
    }
}
