<?php

namespace Tests\Unit\Models;

use App\User;
use App\GiatmuCategory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class GiatmuCategoryTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_giatmu_category_has_name_link_attribute()
    {
        $giatmuCategory = factory(GiatmuCategory::class)->create();

        $title = __('app.show_detail_title', [
            'name' => $giatmuCategory->name, 'type' => __('giatmu_category.giatmu_category'),
        ]);
        $link = '<a href="'.route('giatmu_categories.show', $giatmuCategory).'"';
        $link .= ' title="'.$title.'">';
        $link .= $giatmuCategory->name;
        $link .= '</a>';

        $this->assertEquals($link, $giatmuCategory->name_link);
    }

    /** @test */
    public function a_giatmu_category_has_belongs_to_creator_relation()
    {
        $giatmuCategory = factory(GiatmuCategory::class)->make();

        $this->assertInstanceOf(User::class, $giatmuCategory->creator);
        $this->assertEquals($giatmuCategory->creator_id, $giatmuCategory->creator->id);
    }
}
