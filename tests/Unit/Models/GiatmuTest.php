<?php

namespace Tests\Unit\Models;

use App\User;
use App\Giatmu;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class GiatmuTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_giatmu_has_name_link_attribute()
    {
        $giatmu = factory(Giatmu::class)->create();

        $title = __('app.show_detail_title', [
            'name' => $giatmu->name, 'type' => __('giatmu.giatmu'),
        ]);
        $link = '<a href="'.route('giatmus.show', $giatmu).'"';
        $link .= ' title="'.$title.'">';
        $link .= $giatmu->name;
        $link .= '</a>';

        $this->assertEquals($link, $giatmu->name_link);
    }

    /** @test */
    public function a_giatmu_has_belongs_to_creator_relation()
    {
        $giatmu = factory(Giatmu::class)->make();

        $this->assertInstanceOf(User::class, $giatmu->creator);
        $this->assertEquals($giatmu->creator_id, $giatmu->creator->id);
    }
}
