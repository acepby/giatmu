<?php

return [
    'zoom_level'           => 13,
    'detail_zoom_level'    => 16,
    'map_center_latitude'  => env('MAP_CENTER_LATITUDE', '-2.387992609909441'),
    'map_center_longitude' => env('MAP_CENTER_LONGITUDE', '118.12000000000012'),
];
