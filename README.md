# GiatMu Laravel-leafletjs  

Aplikasi menggunakan [Leaflet JS](https://leafletjs.com) dengan plugin [Markercluster](https://github.com/Leaflet/Leaflet.markercluster),[Markercluster.LayerSupport](https://github.com/ghybs/Leaflet.MarkerCluster.LayerSupport),[Leaflet Slider](https://github.com/dwilhelm89/LeafletSlider) dan [OpenStreetMap](https://www.openstreetmap.org) menggunakan  Laravel 7.

![Giatmu Screenshot](public/screenshot/giatmu_screenshot.png)

## Attibusi

Project ini mengambil code dan inspirasi dari [Laravel Leaflet JS project example](https://github.com/nafiesl/laravel-leaflet-example).

## Installation Steps

Follow this instructions to install the project:

1. Clone this repo.
    ```bash
    $ git clone git@gitlab.com:acepby/giatmu.git
    # or
    $ git clone https://gitlab.com/acepby/giatmu.git
    ```
2. `$ cd giatmu`
3. `$ composer install`
4. `$ cp .env.example .env`
5. `$ php artisan key:generate`
6. Set **database config** on `.env` file
7. `$ php artisan migrate`
8. `$ php artisan serve`
10. Open `https://localhost:8000` with browser.

### Demo Records

untuk model demo dapat menggunakan tinker:

```bash
$ php artisan tinker
>>> factory(App\Giatmu::class, 30)->create();
```


## License

This project is open-sourced software licensed under the [MIT license](LICENSE).
