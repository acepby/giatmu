<?php

namespace App;

use App\User;
use App\Giatmu;
use Illuminate\Database\Eloquent\Model;

class GiatmuCategory extends Model
{
    protected $fillable = ['name', 'description', 'creator_id'];

    public function getNameLinkAttribute()
    {
        $title = __('app.show_detail_title', [
            'name' => $this->name, 'type' => __('giatmu_category.giatmu_category'),
        ]);
        $link = '<a href="'.route('giatmu_categories.show', $this).'"';
        $link .= ' title="'.$title.'">';
        $link .= $this->name;
        $link .= '</a>';

        return $link;
    }

    public function creator()
    {
        return $this->belongsTo(User::class);
    }

    public function giatmu()
    {
	return $this->hasMany(Giatmu::class);
    }
}
