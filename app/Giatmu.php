<?php

namespace App;

use App\User;
use App\GiatmuCategory;
use Illuminate\Database\Eloquent\Model;

class Giatmu extends Model
{
    protected $fillable = ['name', 'description','giatmu_category_id', 'tanggal','lokasi','url','latitude','longitude','creator_id'];

    public $appends = [
        'coordinate', 'map_popup_content',
    ];

    public function getNameLinkAttribute()
    {
        $title = __('app.show_detail_title', [
            'name' => $this->name, 'type' => __('giatmu.giatmu'),
        ]);
        $link = '<a href="'.route('giatmus.show', $this).'"';
        $link .= ' title="'.$title.'">';
        $link .= $this->name;
        $link .= '</a>';

        return $link;
    }

    public function getUrlLinkAttribute()
    {
        $link = '<a href="'.$this->url.'"';
        $link .= ' title="'.$this->url.'">';
        $link .= $this->url;
        $link .= '</a>';

        return $link;
    }

    public function creator()
    {
        return $this->belongsTo(User::class);
    }

    public function category()
    {
	return $this->belongsTo(GiatmuCategory::class,'giatmu_category_id');
    }

    /**
    * Get outlet coordinate attribute.
    *
    * @return string|null
    */
   public function getCoordinateAttribute()
   {
       if ($this->latitude && $this->longitude) {
           return $this->latitude.', '.$this->longitude;
       }
   }
   // copas dari https://github.com/nafiesl/laravel-leaflet-example
   /**
    * Get outlet map_popup_content attribute.
    *
    * @return string
    */
   public function getMapPopupContentAttribute()
   {
       $mapPopupContent = '';
       $mapPopupContent .= '<div class="my-2"><strong>'.__('giatmu.name').':</strong><br>'.$this->name_link.'</div>';
       $mapPopupContent .= '<div class="my-2"><strong>'.__('giatmu.description').':</strong><br>'.$this->description.'</div>';
       $mapPopupContent .= '<div class="my-2"><strong>'.__('giatmu.category').':</strong><br>'.$this->category->name.'</div>';
       $mapPopupContent .= '<div class="my-2"><strong>'.__('giatmu.url').':</strong><br>'.$this->url_link.'</div>';
       $mapPopupContent .= '<div class="my-2"><strong>'.__('giatmu.coordinate').':</strong><br>'.$this->coordinate.'</div>';

       return $mapPopupContent;
   }
}
