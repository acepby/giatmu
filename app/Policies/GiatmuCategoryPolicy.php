<?php

namespace App\Policies;

use App\User;
use App\GiatmuCategory;
use Illuminate\Auth\Access\HandlesAuthorization;

class GiatmuCategoryPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the giatmu_category.
     *
     * @param  \App\User  $user
     * @param  \App\GiatmuCategory  $giatmuCategory
     * @return mixed
     */
    public function view(User $user, GiatmuCategory $giatmuCategory)
    {
        // Update $user authorization to view $giatmuCategory here.
        return true;
    }

    /**
     * Determine whether the user can create giatmu_category.
     *
     * @param  \App\User  $user
     * @param  \App\GiatmuCategory  $giatmuCategory
     * @return mixed
     */
    public function create(User $user, GiatmuCategory $giatmuCategory)
    {
        // Update $user authorization to create $giatmuCategory here.
        return true;
    }

    /**
     * Determine whether the user can update the giatmu_category.
     *
     * @param  \App\User  $user
     * @param  \App\GiatmuCategory  $giatmuCategory
     * @return mixed
     */
    public function update(User $user, GiatmuCategory $giatmuCategory)
    {
        // Update $user authorization to update $giatmuCategory here.
        return true;
    }

    /**
     * Determine whether the user can delete the giatmu_category.
     *
     * @param  \App\User  $user
     * @param  \App\GiatmuCategory  $giatmuCategory
     * @return mixed
     */
    public function delete(User $user, GiatmuCategory $giatmuCategory)
    {
        // Update $user authorization to delete $giatmuCategory here.
        return true;
    }
}
