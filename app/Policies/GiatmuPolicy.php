<?php

namespace App\Policies;

use App\User;
use App\Giatmu;
use Illuminate\Auth\Access\HandlesAuthorization;

class GiatmuPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the giatmu.
     *
     * @param  \App\User  $user
     * @param  \App\Giatmu  $giatmu
     * @return mixed
     */
    public function view(User $user, Giatmu $giatmu)
    {
        // Update $user authorization to view $giatmu here.
        return true;
    }

    /**
     * Determine whether the user can create giatmu.
     *
     * @param  \App\User  $user
     * @param  \App\Giatmu  $giatmu
     * @return mixed
     */
    public function create(User $user, Giatmu $giatmu)
    {
        // Update $user authorization to create $giatmu here.
        return true;
    }

    /**
     * Determine whether the user can update the giatmu.
     *
     * @param  \App\User  $user
     * @param  \App\Giatmu  $giatmu
     * @return mixed
     */
    public function update(User $user, Giatmu $giatmu)
    {
        // Update $user authorization to update $giatmu here.
        return true;
    }

    /**
     * Determine whether the user can delete the giatmu.
     *
     * @param  \App\User  $user
     * @param  \App\Giatmu  $giatmu
     * @return mixed
     */
    public function delete(User $user, Giatmu $giatmu)
    {
        // Update $user authorization to delete $giatmu here.
        return true;
    }
}
