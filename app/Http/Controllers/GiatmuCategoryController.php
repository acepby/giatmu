<?php

namespace App\Http\Controllers;

use App\GiatmuCategory;
use Illuminate\Http\Request;

class GiatmuCategoryController extends Controller
{
    /**
     * Display a listing of the giatmuCategory.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $editableGiatmuCategory = null;
        $giatmuCategoryQuery = GiatmuCategory::query();
        $giatmuCategoryQuery->where('name', 'like', '%'.request('q').'%');
        $giatmuCategories = $giatmuCategoryQuery->paginate(25);

        if (in_array(request('action'), ['edit', 'delete']) && request('id') != null) {
            $editableGiatmuCategory = GiatmuCategory::find(request('id'));
        }

        return view('giatmu_categories.index', compact('giatmuCategories', 'editableGiatmuCategory'));
    }

    /**
     * Store a newly created giatmuCategory in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->authorize('create', new GiatmuCategory);

        $newGiatmuCategory = $request->validate([
            'name'        => 'required|max:60',
            'description' => 'nullable|max:255',
        ]);
        $newGiatmuCategory['creator_id'] = auth()->id();

        GiatmuCategory::create($newGiatmuCategory);

        return redirect()->route('giatmu_categories.index');
    }

    /**
     * Update the specified giatmuCategory in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\GiatmuCategory  $giatmuCategory
     * @return \Illuminate\Routing\Redirector
     */
    public function update(Request $request, GiatmuCategory $giatmuCategory)
    {
        $this->authorize('update', $giatmuCategory);

        $giatmuCategoryData = $request->validate([
            'name'        => 'required|max:60',
            'description' => 'nullable|max:255',
        ]);
        $giatmuCategory->update($giatmuCategoryData);

        $routeParam = request()->only('page', 'q');

        return redirect()->route('giatmu_categories.index', $routeParam);
    }

    /**
     * Remove the specified giatmuCategory from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\GiatmuCategory  $giatmuCategory
     * @return \Illuminate\Routing\Redirector
     */
    public function destroy(Request $request, GiatmuCategory $giatmuCategory)
    {
        $this->authorize('delete', $giatmuCategory);

        $request->validate(['giatmu_category_id' => 'required']);

        if ($request->get('giatmu_category_id') == $giatmuCategory->id && $giatmuCategory->delete()) {
            $routeParam = request()->only('page', 'q');

            return redirect()->route('giatmu_categories.index', $routeParam);
        }

        return back();
    }
}
