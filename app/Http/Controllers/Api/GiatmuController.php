<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Giatmu;
use App\Http\Resources\Giatmu as GiatmuResource;

class GiatmuController extends Controller
{
  public function index(Request $request)
  {
      $giatmus = Giatmu::with('category')->get();

      $geoJSONdata = $giatmus->map(function ($giatmu) {
          return [
              'type'       => 'Feature',
              'properties' => new GiatmuResource($giatmu),
              'geometry'   => [
                  'type'        => 'Point',
                  'coordinates' => [
                      $giatmu->longitude,
                      $giatmu->latitude,
                  ],
              ],
          ];
      });

      return response()->json([
          'type'     => 'FeatureCollection',
          'features' => $geoJSONdata,
      ]);
  }
}
