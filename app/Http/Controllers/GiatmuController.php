<?php

namespace App\Http\Controllers;

use App\Giatmu;
use App\GiatmuCategory;
use Illuminate\Http\Request;

class GiatmuController extends Controller
{
    /**
     * Display a listing of the giatmu.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $editableGiatmu = null;
        $giatmuQuery = Giatmu::query();
        $giatmuQuery->where('name', 'like', '%'.request('q').'%');
        $giatmus = $giatmuQuery->paginate(25);

        if (in_array(request('action'), ['edit', 'delete']) && request('id') != null) {
            $editableGiatmu = Giatmu::find(request('id'));
        }

        return view('giatmus.index', compact('giatmus', 'editableGiatmu'));
    }

    public function create()
    {
        $this->authorize('create', new Giatmu);
        $data['categories'] = GiatmuCategory::get();

        return view('giatmus.create',$data);
    }

    /**
     * Store a newly created giatmu in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->authorize('create', new Giatmu);
        //dd($request);
        $newGiatmu = $request->validate([
            'name'        => 'required|max:60',
            'description' => 'nullable|max:255',
            'giatmu_category_id' => 'required',
            'tanggal'           => 'required',
            'lokasi'            => 'required',
            'url'               => 'required',
            'latitude'          => 'nullable|required_with:longitude|max:15',
            'longitude'         => 'nullable|required_with:latitude|max:15',
        ]);
        $newGiatmu['creator_id'] = auth()->id();

        Giatmu::create($newGiatmu);

        return redirect()->route('giatmus.index');
    }



    public function show(Giatmu $giatmu)
    {
        return view('giatmus.show', compact('giatmu'));
    }


    public function edit(Giatmu $giatmu)
    {
        $this->authorize('update', $giatmu);
        $data['categories'] = GiatmuCategory::get();

        return view('giatmus.edit', compact('giatmu','data'));
    }

    public function update(Request $request, Giatmu $giatmu)
    {
        $this->authorize('update', $giatmu);

        $giatmuData = $request->validate([
          'name'        => 'required|max:60',
          'description' => 'nullable|max:255',
          'giatmu_category_id' => 'required',
          'tanggal'           => 'required',
          'lokasi'            => 'required',
          'url'               => 'required',
          'latitude'          => 'nullable|required_with:longitude|max:15',
          'longitude'         => 'nullable|required_with:latitude|max:15',
        ]);
        $giatmu->update($giatmuData);

        return redirect()->route('giatmus.show', $giatmu);
    }



    /**
     * Remove the specified giatmu from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Giatmu  $giatmu
     * @return \Illuminate\Routing\Redirector
     */
    public function destroy(Request $request, Giatmu $giatmu)
    {
        $this->authorize('delete', $giatmu);

        $request->validate(['giatmu_id' => 'required']);

        if ($request->get('giatmu_id') == $giatmu->id && $giatmu->delete()) {
            $routeParam = request()->only('page', 'q');

            return redirect()->route('giatmus.index', $routeParam);
        }

        return back();
    }
}
