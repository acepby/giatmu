<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Acara extends Model
{
    protected $fillable = ['nama','description','tanggal','lokasi','url','latitude','longitude','user_id'];
}
